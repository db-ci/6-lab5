import { Livro } from "../entidades/livro";
import { LivroModel } from "./livroModel";
import { AutorModel } from "./autorModel";

export class LivroRepositorio {
    static async criar(livro: Livro): Promise<Livro> { //4.a
        return LivroModel.create(livro); //retorna uma Promise
    }

    static async buscar(): Promise<Livro[]> { //4.b
        return LivroModel.find().populate('autores', AutorModel).exec();
    }

    static async buscarPorIdAutor(id: string): Promise<Livro[]> { //4.c
        return LivroModel.where('autores').equals(id).populate('autores', AutorModel).exec();
    }

    static async buscarPorIdLivro(id: string): Promise<Livro|null> { 
        return LivroModel.findById(id).exec();
    }
}