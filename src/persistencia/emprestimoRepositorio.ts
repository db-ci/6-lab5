import { Emprestimo } from "../entidades/emprestimo";
import { EmprestimoModel } from "./emprestimoModel";

export class EmprestimoRepositorio { //inserir um novo empréstimo
    static async criar(emprestimo: Emprestimo): Promise<Emprestimo> {
        return EmprestimoModel.create(emprestimo); //retorna uma Promise
    }

    static async buscar(): Promise<Emprestimo[]> {
        return EmprestimoModel.find().exec();
    }
}