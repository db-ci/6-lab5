import { Autor } from "../entidades/autor";
import { AutorModel } from "./autorModel";

export class AutorRepositorio {
    static async criar(autor: Autor): Promise<Autor> { 
        return AutorModel.create(autor); //retorna uma Promise
    }
    // não precisa de await pq retorna promisse direto, 
    // create() ta na mesma linha do return
    // let novoAutor = await AutorModel.create(autor);
    // return novoAutor;
    // precisa do await pq não ta retornando direto

    static async buscar(): Promise<Autor[]> {
        let consulta = AutorModel.find();
        return consulta.exec(); //retorna uma Promise
    }
    //se o exec tivesse na linha onde não tem return, precisaria do await
    //let consulta = await AutorModel.find().exec()
    //return consulta

    static async buscarUltimoNome(nome: string): Promise<Autor[]> { //a
        let consulta_ultimo = AutorModel.find().where('ultimo_nome').equals(nome);
        return consulta_ultimo.exec(); 
    }

    static async buscarPrimeiroNome(nome: string): Promise<Autor[]> { //b
        let consulta_primeiro = AutorModel.find().where('primeiro_nome').equals(nome);
        return consulta_primeiro.exec(); 
    }

    static async alterarAutor(autor: Autor, id: string): Promise<Autor> { //c
        let autorAtual = await AutorModel.findById(id).exec();
        if (autorAtual !== null) {
            autorAtual.primeiro_nome = autor.primeiro_nome;
            autorAtual.ultimo_nome = autor.ultimo_nome;
            return autorAtual.save();
        } else {
            throw new Error('Id inexistente'); 
            //ou return Promise.reject('Id inexistente');
        }
    }
}
