import {connect} from 'mongoose';
import { AutorRepositorio } from './persistencia/autorRepositorio';
import { LivroRepositorio } from './persistencia/livroRepositorio';

async function main() {
    const url = 'mongodb://localhost:27017/biblioteca';
    try {
        const cliente = await connect(url, { useNewUrlParser: true, useUnifiedTopology: true });
        console.log('Conectado com sucesso')
        /*
        console.log('Adicionando autores...');
        let a1 = await AutorRepositorio.criar({primeiro_nome: 'John', ultimo_nome: 'Doe'});
        console.log(`Autor inserido: ${a1}`);
        let a2 = await AutorRepositorio.criar({primeiro_nome: 'Mary', ultimo_nome: 'Doe'});
        console.log(`Autor inserido: ${a2}`);

        console.log('Buscando autores...');
        let autores = await AutorRepositorio.buscar();
        autores.forEach(a => console.log(autor));
        */
        let autores = await AutorRepositorio.buscar();
        /*
        console.log('Adicionando livros...');
        let l1 = await LivroRepositorio.criar({
            titulo: 'Livro 1',
            autores: [autores[0], autores[1]]
        });
        console.log('Livro inserido: ${l1}');

        let l2 = await LivroRepositorio.criar({
            titulo: 'Livro 2',
            autores: [autores[0]]
        });
        console.log('Livro inserido: ${l2}');
        
        console.log('Buscando livros...');
        let livros = await LivroRepositorio.buscar();
        livros.forEach(l => console.log(l));
        */
        let livros = await LivroRepositorio.buscarPorIdAutor('5f0f6e756f31ce17908a0a22');
        console.log(livros);

        if (cliente && cliente.connection) {
            cliente.connection.close();
            console.log('Desconectado');
        }
    } catch (error) {
        console.log(`Erro: ${error}`);
    }
}

main();